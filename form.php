<body onload="advancedSettings();">
<link rel="style.css" href="form.css" type="text">
<div class="personal" >
    <form>
        <fieldset>
            <legend>REGISTRATION</legend>
            <br>
            <label for="name">Name:*</label><br>
            <input type="text" id="name" name="name" required>
            <br><br>
            <label for="username">Username:*</label><br>
            <input type:"text" id="username" name="username" required>
            <br><br>
            <label for="password">Password:*</label><br>
            <input type="password" id="password1" name="password" required>
            <br><br>
            <label for="password">Password again:* </label><br>
            <input type="password" id="password2" name="password1" onchange="check()"required>
            <br><br>
            <label for ="email">Email:* </label><br>
            <input type="email" id="email" name="email"    placeholder="   example@example.com" required></label>
            <br><br>
            <label for="birth">Date of birth: </label><br>
            <input type="date" id="birth" value="birth" >
            <br><br>
            <label for="settings">Advanced settings </label>
            <input type="checkbox" name="settings" id="settings" value="yes" onchange="advancedSettings()">

            <div id="advanced">
                <br><br>
                <label for="radio">Gender: </label>
                <input type="radio" id="radio" name="gender"   value="female"> Female
                <input type="radio" id="radio" name="gender" value="male"> Male
                <br><br>
                <label for="radio">Occupation: </label><br>
                <input type="checkbox" id="occupation" name="checkbox" value="Student">Student
                <input type="checkbox" id="occupation" name="checkbox" value="Employee">Employee
                <input type="checkbox" id="occupation" name="checkbox" value="Unemployed">Unemployed
                <br><br>
                <label for="children">Children: </label>
                <input type="number" id="children" name="children">
                <br><br>
                <label for="color">Choose a color: </label>
                <input type="color" id="color" name="color" value="#ffb380">
                <br><br>
                <label for="range">How do you feel today: </label><br>
                Sad <input type="range" id="range" name="range"> Happy
            </div> <!-- / advanced -->
            <br><br>
            <input type="submit" value="Send" onclick="return check();">
        </fieldset>
    </form>
</div> <!-- / personal -->

<div class="billing">
    <form>
        <fieldset>
            <legend>BILLING INFORMATION</legend>
            <br>
            <label for="name">Name:*</label><br>
            <input type="text" id="name" name="name" required>
            <br><br>
            <label for ="email">Email:* </label><br>
            <input type="email" id="email" name="email"    placeholder="   example@example.com" required></label>
            <br><br>
            <label for ="shippingName">Shipping Address:</label><br>
            <input type = "text" name = "shipName" id = "shippingName" required><br/><br>
            <input type = "text" name = "shipZip" id = "shippingZip" pattern = "[0-9]{5}" placeholder="               zip code" required><br/><br>

            <input type="checkbox" id="same" name="same" onchange= "billingFunction()"/>
            <label for = "same">Is the billing address the same?</label><br><br>

            <label for ="billingName">Billing Address:</label><br>
            <input type = "text" name = "billName" id = "billingName" required><br/><br>
            <input type = "text" name = "billZip" id = "billingZip" pattern = "[0-9]{5}" placeholder="               zip code" required><br/>
            <br><br><br>
            <input type="submit" value="Submit" onclick="return check();">
        </fieldset>
    </form>
</body>