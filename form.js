function check() {
    var password1 = document.getElementById("password1");
    var password2 = document.getElementById("password2");
    if (password1.value != password2.value)   {
        alert("Passwords must match!");
        return false;
    }
}

function advancedSettings() {
    if (document.getElementById("settings").checked) {
        document.getElementById("advanced").style.display="inline"
    } else {   document.getElementById("advanced").style.display="none";
    }
}

function billingFunction() {
    var shipName = document.getElementById("shippingName");
    var shipZip = document.getElementById("shippingZip");
    if (document.getElementById("same").checked) {
        document.getElementById("billingName").value = shipName.value;
        document.getElementById("billingZip").value = shipZip.value;
    } else {
        document.getElementById("billingName").value = "";
        document.getElementById("billingZip").value = "";
    }
}